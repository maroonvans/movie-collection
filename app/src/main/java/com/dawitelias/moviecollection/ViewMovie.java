package com.dawitelias.moviecollection;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by dxe on 4/11/2015.
 */
public class ViewMovie extends ActionBarActivity {
    private long rowID; // selected movie that was passed from prev. activity

    // using the ButterKnife injection library for cleaner code
    @InjectView(R.id.titleTextView) TextView movieTitleTextView;
    @InjectView(R.id.yearTextView) TextView movieYearTextView;
    @InjectView(R.id.directorTextView) TextView movieDirectorTextView;
    @InjectView(R.id.genreTextView) TextView movieGenreTextView;
    @InjectView(R.id.starsTextView) TextView movieStarringTextView;
    @InjectView(R.id.ratingTextView) TextView movieRatingTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_movie);
        ButterKnife.inject(this);

        // get passed row ID from prev activity and assign it to rowID
        Bundle extras = getIntent().getExtras();
        rowID = extras.getLong(MovieCollection.ROW_ID);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // new async task to load data
        new LoadMovieTask().execute(rowID);
    }

    private class LoadMovieTask extends AsyncTask<Long, Object, Cursor> {
        DatabaseConnector dbConn = new DatabaseConnector(ViewMovie.this);

        // access db
        @Override
        protected Cursor doInBackground(Long... params) {
            dbConn.open(); // op db

            // get cursor containing all data for specific entry
            return dbConn.getOneMovie(params[0]);
        }

        @Override
        protected void onPostExecute(Cursor result) {
            super.onPostExecute(result);

            result.moveToFirst(); // start cursor at first position

            // get column index
            int titleIndex = result.getColumnIndex("title");
            int yearIndex = result.getColumnIndex("year");
            int directorIndex = result.getColumnIndex("director");
            int genreIndex = result.getColumnIndex("genre");
            int starringIndex = result.getColumnIndex("starring");
            int ratingIndex = result.getColumnIndex("rating");

            // populate textviews with data
            movieTitleTextView.setText(result.getString(titleIndex));
            movieYearTextView.setText(result.getString(yearIndex));
            movieDirectorTextView.setText(result.getString(directorIndex));
            movieGenreTextView.setText(result.getString(genreIndex));
            movieStarringTextView.setText(result.getString(starringIndex));
            movieRatingTextView.setText(result.getString(ratingIndex) + "/5 stars");

            result.close(); // close cursor
            dbConn.close(); // close db
        }
    } // close LoadMovieTask

    // delete a contact
    private void deleteMovie()
    {
        // create a new AlertDialog Builder
        AlertDialog.Builder builder =
                new AlertDialog.Builder(ViewMovie.this);

        builder.setTitle(R.string.confirmTitle); // title bar string
        builder.setMessage(R.string.confirmMessage); // message to display

        // provide an OK button that simply dismisses the dialog
        builder.setPositiveButton(R.string.button_delete,
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int button)
                    {
                        final DatabaseConnector databaseConnector =
                                new DatabaseConnector(ViewMovie.this);

                        // create an AsyncTask that deletes the movie in another
                        // thread, then calls finish after the deletion
                        AsyncTask<Long, Object, Object> deleteTask =
                                new AsyncTask<Long, Object, Object>()
                                {
                                    @Override
                                    protected Object doInBackground(Long... params)
                                    {
                                        databaseConnector.deleteMovie(params[0]);
                                        return null;
                                    } // end method doInBackground

                                    @Override
                                    protected void onPostExecute(Object result)
                                    {
                                        finish(); // return to the MovieCollection Activity
                                    } // end method onPostExecute
                                }; // end new AsyncTask

                        // execute the AsyncTask to delete contact at rowID
                        deleteTask.execute(new Long[] { rowID });
                    } // end method onClick
                } // end anonymous inner class
        ); // end call to method setPositiveButton

        builder.setNegativeButton(R.string.button_cancel, null);
        builder.show(); // display the Dialog
    } // end method deleteMovie

    // create the Activity's menu from a menu resource XML file
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view_movie_menu, menu);
        return true;
    }

    // handle choice from options menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) // switch based on selected MenuItem's ID
        {
            case R.id.editItem:
                // create an Intent to launch the AddEditContact Activity
                Intent addEditMovie =
                        new Intent(this, AddEditMovie.class);

                // pass the selected contact's data as extras with the Intent
                addEditMovie.putExtra(MovieCollection.ROW_ID, rowID);
                addEditMovie.putExtra("title", movieTitleTextView.getText());
                addEditMovie.putExtra("year", movieYearTextView.getText());
                addEditMovie.putExtra("director", movieDirectorTextView.getText());
                addEditMovie.putExtra("genre", movieGenreTextView.getText());
                addEditMovie.putExtra("starring", movieStarringTextView.getText());
                addEditMovie.putExtra("rating", movieRatingTextView.getText());
                startActivity(addEditMovie); // start the Activity
                return true;
            case R.id.deleteItem:
                deleteMovie(); // delete the displayed contact
                return true;
            default:
                return super.onOptionsItemSelected(item);
        } // end switch
    }

}
