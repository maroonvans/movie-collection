package com.dawitelias.moviecollection;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask; // good for once and one data fetching
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;


public class MovieCollection extends ActionBarActivity {

    // key-value pair passed between activities
    public static final String ROW_ID = "row_id";

    private ListView movieListView; // so we can deal with it programmatically

    // adapter populates ListView
    private CursorAdapter movieAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize gui elements and set listeners
        movieListView = (ListView) findViewById(R.id.listView);
        movieListView.setOnItemClickListener(viewMovieListener);
        movieListView.setBackgroundColor(Color.BLACK);

        // display empty list message
        TextView emptyText = (TextView) View.inflate(this, R.layout.movie_list_empty, null);
        emptyText.setVisibility(View.GONE);
        ((ViewGroup)movieListView.getParent()).addView((emptyText));
        movieListView.setEmptyView(emptyText); // when view is empty, set text

        // map each movie title to a TextView in the ListView
        String[] from = new String[]{"title"};
        int[] to = new int[]{R.id.movieTextView};

        movieAdapter = new SimpleCursorAdapter(MovieCollection.this,R.layout.movie_list_item,null,from,to,0);
        movieListView.setAdapter(movieAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // create a new GetContactsTask (async task) and execute
        new GetMoviesTask().execute((Object[]) null);
    }

    @Override
    protected void onStop() {
        Cursor cursor = movieAdapter.getCursor(); // get current cursor
        if (cursor != null) {
            cursor.close(); // deactivate it
        }

        movieAdapter.changeCursor(null); // adapter not has no cursor (freeing up memory)
        super.onStop();
    }

    // database query (not on gui thread, better for performance)
    private class GetMoviesTask extends AsyncTask<Object,Object,Cursor> {
        DatabaseConnector dbConn = new DatabaseConnector(MovieCollection.this);

        // access db
        @Override
        protected Cursor doInBackground(Object... params) {
            dbConn.open(); // open db
            return dbConn.getAllMovies(); // get cursor with all movies
        }

        // use returned cursor and apply to adapter
        @Override
        protected void onPostExecute(Cursor result) {
            movieAdapter.changeCursor(result);
            dbConn.close(); // close db
        }
    }

    // launch new activity when movie item clicked
    OnItemClickListener viewMovieListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // create new intent to launch view movie activity
            Intent viewMovie = new Intent(MovieCollection.this, ViewMovie.class);

            // pass row id of movie to next activity via the intent
            viewMovie.putExtra(ROW_ID,id);
            startActivity(viewMovie);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie_collection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // create intent to launch AddEditMovie activity
        Intent addNewMovie = new Intent(MovieCollection.this, AddEditMovie.class);
        startActivity(addNewMovie);

        return super.onOptionsItemSelected(item);
    }
}
