package com.dawitelias.moviecollection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
/**
 * Created by dxe on 4/11/2015.
 */
public class DatabaseConnector {
    // database name
    private static final String DATABASE_NAME = "UserMovies";
    private SQLiteDatabase database; // create database object

    // database helper class to run sql queries
    private DatabaseOpenHelper databaseOpenHelper;

    public DatabaseConnector(Context context) {
        // create new DatabaseOpenHelper
        databaseOpenHelper = new DatabaseOpenHelper(context, DATABASE_NAME, null, 1);
    }

    // open database connection
    public void open() throws SQLException {
        // create or open a database for reading/writing
        database = databaseOpenHelper.getWritableDatabase();
    }

    // terminate db connection
    public void close() {
        if (database != null) { database.close(); }
    }

    // insert new movie
    public void insertMovie(String movieTitle, String movieYear, String movieDirector, String movieGenre, String starring, float rating) {
        ContentValues newMovie = new ContentValues();
        newMovie.put("title", movieTitle);
        newMovie.put("year", movieYear);
        newMovie.put("director", movieDirector);
        newMovie.put("genre", movieGenre);
        newMovie.put("starring", starring);
        newMovie.put("rating", rating);

        open(); // open db
        database.insert("movies", null, newMovie);
        close(); // close db
    }

    // update new movie
    public void updateMovie(long id, String movieTitle, String movieYear, String movieDirector, String movieGenre, String starring, float rating) {
        ContentValues editMovie = new ContentValues();
        editMovie.put("title", movieTitle);
        editMovie.put("year", movieYear);
        editMovie.put("director", movieDirector);
        editMovie.put("genre", movieGenre);
        editMovie.put("starring", starring);
        editMovie.put("rating", rating);

        open(); // open db
        database.update("movies", editMovie, "_id=" + id, null);
        close(); // close db
    }

    // delete the contact specified by the given String title
    public void deleteMovie(long id) {
        open(); // open db
        database.delete("movies", "_id=" + id, null);
        close(); // close db
    }

    // return a Cursor with all contact information in the database
    public Cursor getAllMovies() {
        return database.query("movies", new String[] {"_id","title"},
                null,null,null,null,"title");
    }

    // get a Cursor containing all information about the contact specified
    // by the given id
    public Cursor getOneMovie(long id) {
        return database.query("movies",null,"_id=" + id,
                null,null,null,null);
    }

    private class DatabaseOpenHelper extends SQLiteOpenHelper {
        public DatabaseOpenHelper(Context context, String title, CursorFactory factory,
                                  int version) {
            super(context, title, factory, version);
        }

        // create movies table when db is created
        @Override
        public void onCreate(SQLiteDatabase db) {
            // sql query to create table
            String query = "CREATE TABLE movies" + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "title TEXT, year TEXT, director TEXT, genre TEXT, starring TEXT, " +
                    "rating REAL);";
            db.execSQL(query);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){}
    }



}
