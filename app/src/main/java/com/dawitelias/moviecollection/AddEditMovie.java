package com.dawitelias.moviecollection;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by dxe on 4/11/2015.
 */
public class AddEditMovie extends Activity {
    private long rowID; // selected movie that was passed from prev. activity

    // using the ButterKnife injection library for cleaner code
    @InjectView(R.id.movieTitleEditText) EditText movieTitleEditText;
    @InjectView(R.id.yearEditText) EditText movieYearEditText;
    @InjectView(R.id.directorEditText) EditText movieDirectorEditText;
    @InjectView(R.id.genreEditText) EditText movieGenreEditText;
    @InjectView(R.id.starsEditText) EditText movieStarringEditText;
    @InjectView(R.id.ratingBar) RatingBar movieRatingRatingBar;
    @InjectView(R.id.saveMovieButton) Button saveMovieButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_movie);
        ButterKnife.inject(this);
        Bundle extras = getIntent().getExtras();

        // if there ar extras, use them to populate the EditTexts
        if (extras != null) {
            rowID = extras.getLong("row_id");
            movieTitleEditText.setText(extras.getString("title"));
            movieYearEditText.setText(extras.getString("year"));
            movieDirectorEditText.setText(extras.getString("director"));
            movieGenreEditText.setText(extras.getString("genre"));
            movieStarringEditText.setText(extras.getString("starring"));
//            movieRatingRatingBar.setNumStars(extras.getRating());
        }

        // set event listener for the Save Contact Button
        saveMovieButton.setOnClickListener(saveMovieButtonClicked);
    }

    OnClickListener saveMovieButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (movieTitleEditText.getText().length() != 0) {
                AsyncTask<Object, Object, Object> saveMovieTask =
                        new AsyncTask<Object, Object, Object>()
                        {
                            @Override
                            protected Object doInBackground(Object... params)
                            {
                                saveMovie(); // save movie to the database
                                return null;
                            } // end method doInBackground

                            @Override
                            protected void onPostExecute(Object result)
                            {
                                finish(); // return to the previous Activity
                            } // end method onPostExecute
                        }; // end AsyncTask

                // save the movie to the database using a separate thread
                saveMovieTask.execute((Object[]) null);
            } else {
                // create a new AlertDialog Builder
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(AddEditMovie.this);

                // set dialog title & message, and provide Button to dismiss
                builder.setTitle(R.string.errorTitle);
                builder.setMessage(R.string.errorMessage);
                builder.setPositiveButton(R.string.errorButton, null);
                builder.show(); // display the Dialog
            } // end else
        } // end onclick
    }; // end onclick listener

    // save new movie info
    private void saveMovie() {
        DatabaseConnector dbConn = new DatabaseConnector(this);

        if (getIntent().getExtras() == null) {
            dbConn.insertMovie(
                movieTitleEditText.getText().toString(),
                movieYearEditText.getText().toString(),
                movieDirectorEditText.getText().toString(),
                movieGenreEditText.getText().toString(),
                movieStarringEditText.getText().toString(),
                movieRatingRatingBar.getRating());
        } else {
            dbConn.updateMovie(rowID,
                    movieTitleEditText.getText().toString(),
                    movieYearEditText.getText().toString(),
                    movieDirectorEditText.getText().toString(),
                    movieGenreEditText.getText().toString(),
                    movieStarringEditText.getText().toString(),
                    movieRatingRatingBar.getRating());
        }
    }
}
